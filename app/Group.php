<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function type() {
        return $this->hasOne('App\GroupType', 'id', 'group_type_id');
    }

    public function members() {
        return $this->belongsToMany('App\User', 'group_members')->withPivot('status', 'right_id')->withTimestamps();
    }

    public static function myGroups($id) {
        return DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->join('group_types', 'groups.group_type_id', '=', 'group_types.id')
            ->select(
                'groups.*',
                'group_members.status',
                'group_types.type'
            )
            ->where('group_members.user_id', '=', $id)
            ->where('group_members.status', '=', 1)
            ->get();
    }

    public static function myPendingGroups($id) {
        return DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->join('group_types', 'groups.group_type_id', '=', 'group_types.id')
            ->select(
                'groups.*',
                'group_members.status',
                'group_types.type'
            )
            ->where('group_members.user_id', '=', $id)
            ->where('group_members.status', '=', 0)
            ->get();
    }

    public static function otherGroups($id) {
        return DB::table('groups')
            ->leftJoin('group_members', 'groups.id', '=', 'group_members.group_id')
            ->join('group_types', 'groups.group_type_id', '=', 'group_types.id')
            ->select(
                'groups.*',
                'group_members.status',
                'group_types.type'
            )
            ->whereNotIn('groups.id',
                DB::table('group_members')
                    ->select('group_members.group_id')
                    ->where('user_id', '=', $id)
            )
            ->get();
    }
}
