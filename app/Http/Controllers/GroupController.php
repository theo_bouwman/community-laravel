<?php

namespace App\Http\Controllers;

use App\Right;
use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Group;
use App\GroupType;
use DB;

class GroupController extends Controller
{
    public function index() {
        $mygroups = Group::myGroups(Auth::user()->id);
        $pendinggroups = Group::myPendingGroups(Auth::user()->id);
        $group = Group::otherGroups(Auth::user()->id);
        $types = GroupType::all();

        return view('groups.index', [
            'groups' => $group,
            'myGroups' => $mygroups,
            'pendingGroups' => $pendinggroups,
            'group_types' => $types
        ]);

    }

    public function detail($id) {

        $group = Group::where('id', $id)->first();
        $types = GroupType::all();
        $admin = false;

        if (!$group) {
            return redirect()->route('group.index')->with(['message' => 'Sorry. Maar we kunnen deze groep niet vinden.']);
        }

        $member = $group
            ->members
            ->where('id', Auth::user()->id)
            ->first();


        // If there is a record with user_id
        if (count($member) == 1) {
            // If status is on hold
            if ($member->pivot->status == 0) {
                return redirect()->route('group.index')->with(['message' => 'Wacht op goedkeuring van je aanvraag.']);
            } elseif ($member->pivot->status == 1) {
                // If user has group admin rights
                if ($group->members->where('id', Auth::user()->id)->first()->pivot->right_id == 2) {
                    $admin = true;
                }

                return view('groups.group', [
                    'group' => $group,
                    'group_types' => $types,
                    'admin' => $admin
                ]);
            }
        }

        return redirect()->route('group.index')->with(['message' => 'Je bent nog geen lid van deze groep.']);
    }

    public function newGroup(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:40|unique:groups',
            'location' => 'required|max:120',
            'description' => 'required',
            'type' => 'required|max:1'
        ]);

        $group = new Group();
        $group->name = $request->input('name');
        $group->location = $request->input('location');
        $group->group_type_id = $request->input('type');
        $group->description = $request->input('description');
        $group->save();

        // Getting group id
        $groupId = Group::where('name', $request->input('name'))->first()->id;

        // Make member
        $group->members()->attach(Auth::user()->id, ['group_id' => $groupId, 'status' => 1, 'right_id' => 2]);

        return redirect()->route('group.detail', ['id' => $groupId])->with(['message' => 'De groep: '. $group->name . ' is aangemaakt.']);
    }

    public function becomeMember($id) {
        $group = Group::find($id);
        if ($group->members()->where('user_id', Auth::user()->id)->first()) {
            return redirect()->route('group.detail', ['id' => $id]);
        }

        switch ($group->type->id) {
            case '1':
                $group->members()->attach(Auth::user()->id, [
                    'group_id' => $id,
                    'status' => 1,
                    'right_id' => 1
                ]);
                return redirect()->route('group.detail', ['id' => $id])->with(['message' => 'Je bent nu lid van deze groep.']);
                break;
            case '2':
                $group->members()->attach(Auth::user()->id, [
                    'group_id' => $id,
                    'status' => 0,
                    'right_id' => 1
                ]);
                return redirect()->route('group.detail', ['id' => $id])->with(['message' => 'Je aanvraag is geplaatst, wacht op goedkeuring.']);
                break;
            case '3':
                return redirect()->back()->with(['message' => 'Je moet een uitnodiging krijgen om lid te kunnen worden.']);
                break;
            default:
                return 'error';
        }
    }

    public function leave($id, $uid = null) {
        $group = Group::find($id);
        $message = 'U heeft deze gebruiker verwijderd van deze groep.';

        if (!$group) {
            return redirect()->back();
        }

        // If user leaved group
        if (is_null($uid)) {
            $uid = Auth::user()->id;
            $message = 'U heeft deze groep verlaten.';
        }

        $group->members()->detach($uid, ['group_id' => $id]);

        return redirect()->route('group.index')->with(['message' => $message]);
    }

    public function edit(Request $request) {
        $this->validate($request, [
            'location' => 'required|max:120',
            'description' => 'required',
            'type' => 'required|max:1'
        ]);

        $group = Group::find($request->input('group_id'));

        if ($group->members()->where('user_id', Auth::user()->id)->first()->pivot->right_id != 2) {
            return redirect()->route('group.detail', ['id' => $request->input('group_id')]);
        }

        $group->description = $request->input('description');
        $group->location= $request->input('location');
        $group->group_type_id = $request->input('type');
        $group->update();

        return redirect()->back()->with(['message' => 'Groepsinstellingen zijn aangepast.']);

    }
}
