<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Auth;
use App\User;

class UserController extends Controller
{
    public function index($id) {
        $user = User::where('id', $id)->first();
        return view('users.profile', [
            'user' => $user
        ]);
    }

    public function updateUserData(Request $request) {
        $this->validate($request, [
            'email' => 'required|max:40',
            'phone' => 'required|max:12',
            'zip_code' => 'required|max:10',
            'adress' => 'required|max:40',
            'city' => 'required|max:40'
        ]);

        $user = Auth::user();

        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->zip_code = $request->input('zip_code');
        $user->adress = $request->input('adress');
        $user->city = $request->input('city');
        $user->update();

        $file = $request->file('image');
        $filename = $user->fname . '-' . $user->id . '.jpg';
        if ($file) {
            Storage::disk('local')->put($filename, File::get($file));
        }

        return redirect()->route('profile', ['id' => Auth::user()->id]);
    }

    public function getUserImage($filename) {
        $file = Storage::disk('local')->get($filename);
        return new Response($file, 200);
    }
}
