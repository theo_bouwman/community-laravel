<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', 'HomeController@index');

    Route::get('/test', function() {

    });

    Route::get('/profile/{id}', [
        'uses' => 'UserController@index',
        'as' => 'profile'
    ]);

    Route::get('/profile-image/{filename}', [
        'uses' => 'UserController@getUserImage',
        'as' => 'profile.image'
    ]);

    Route::post('/edit-profile', [
        'uses' => 'UserController@updateUserData',
        'as' => 'profile.update'
    ]);

    // Groups
    Route::get('/groups', [
        'uses' => 'GroupController@index',
        'as' => 'group.index'
    ]);

    Route::get('/groups/{id}', [
        'uses' => 'GroupController@detail',
        'as' => 'group.detail'
    ]);

    Route::get('/become-group-member/{id}', [
        'uses' => 'GroupController@becomeMember',
        'as' => 'group.becomeMember'
    ]);

    Route::post('/groups/new', [
        'uses' => 'GroupController@newGroup',
        'as' => 'group.new'
    ]);

    Route::post('/groups/edit', [
        'uses' => 'GroupController@edit',
        'as' => 'group.edit'
    ]);

    Route::get('/leave-group/{id}/{uid?}', [
        'uses' => 'GroupController@leave',
        'as' => 'group.leave'
    ]);
});