@extends('layouts.app')

@section('content')

    <div class="col-md-6">
        <div class="panel panel-default">
              <div class="panel-heading">
                    <h3 class="panel-title">
                        {{ $group->name }}
                        @if($admin == true)
                            <span class="badge">Admin</span>
                        @endif
                        <div class="pull-right">
                            <div class="btn-group">
                                <a href="{{ route('group.leave', ['id' => $group->id]) }}" class="btn btn-danger btn-xs">Groep verlaten</a>
                                {{-- If user is group admin --}}
                                @if($admin == true)
                                    <button type="button" class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a data-toggle="modal" href="#editModalAdmin">Groep bewerken</a>
                                        </li>
                                    </ul>
                                @endif
                            </div>

                        </div>
                    </h3>
              </div>
              <div class="panel-body">
                  <ul>
                      <li>Beschrijving: {{ $group->description }}</li>
                      <li>Locatie: {{ $group->location }}</li>
                      <li>Type: {{ $group->type->type }}</li>
                  </ul>
              </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Leden <span class="badge">{{ count($group->members) }}</span></h3>
            </div>
            <div class="panel-body">
                <ul>
                    @foreach($group->members as $user)
                        <li>
                            <a href="{{ route('profile', ['id' => $user->id]) }}">
                                @if($user->id == Auth::user()->id)
                                    Jij
                                @else
                                    {{ $user->fname }} {{ $user->sname }}
                                @endif
                            </a>
                            @if($user->id != Auth::user()->id && $admin == true)
                                <a class="pull-right btn btn-danger btn-xs" href="{{ route('group.leave', ['id' => $group->id, 'uid' => $user->id]) }}">Verwijderen</a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>


    {{-- Model if user is admin of group --}}
    @if($admin == true)
        <div class="modal fade" id="editModalAdmin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Groep bewerken</h4>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('group.edit') }}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="group_id" value="{{ $group->id }}">
                            <div class="form-group">
                                <label>Groeps beschrijving</label>
                                <textarea name="description" class="form-control" required>{{ $group->description }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Groeps locatie</label>
                                <input type="text" name="location" class="form-control" required value="{{ $group->location }}">
                            </div>

                            <div class="form-group">
                                <label>Groeps type</label>
                                <select name="type" class="form-control" required>
                                    @foreach($group_types as $type)
                                        @if($group->type->id == $type->id)
                                            <option value="{{ $type->id }}" selected>{{ $type->type }}: {{ $type->description }}</option>
                                        @else
                                            <option value="{{ $type->id }}">{{ $type->type }}: {{ $type->description }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
                            <button type="submit" class="btn btn-primary">Aanpassingen opslaan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection