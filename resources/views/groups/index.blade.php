@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <button class="btn btn-primary" data-toggle="modal" data-target="#createGroupModal">Nieuwe groep</button>
            </div>
        </div>
    </div>

    <div class="col-md-12">

        @if(count($myGroups) > 0)
            <div class="panel panel-default">
                  <div class="panel-heading">
                        <h3 class="panel-title">Groepen waar ik lid van ben</h3>
                  </div>
                  <div class="panel-body">
                        <ul>
                            @foreach($myGroups as $group)
                                <li>
                                    <a href="{{ route('group.detail', ['id' => $group->id]) }}">
                                        {{ $group->name }}
                                    </a>
                                    <span class="badge">{{ $group->type }}</span>
                                </li>
                            @endforeach
                        </ul>
                  </div>
            </div>
        @endif

        @if(count($pendingGroups) > 0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Groepen met een aanvraag van jou</h3>
                </div>
                <div class="panel-body">
                    <ul>
                        @foreach($pendingGroups as $group)
                            <li>
                                <a href="{{ route('group.detail', ['id' => $group->id]) }}">
                                    {{ $group->name }}
                                </a>
                                <a href="{{ route('group.leave', ['id' => $group->id]) }}" class="pull-right">
                                    <button class="btn btn-danger btn-xs">Aanvraag intrekken</button>
                                </a>
                                <span class="badge">{{ $group->type }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        @if(count($groups) > 0)
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <h3 class="panel-title pull-left">Groepen</h3>
                </div>
                <div class="panel-body">
                    <ul>
                        @foreach($groups as $group)
                            <li>
                                <a href="{{ route('group.detail', ['id' => $group->id]) }}">
                                    {{ $group->name }}
                                </a>
                                <span class="badge">{{ $group->type }}</span>
                                @if($group->group_type_id === 1)
                                    <a href="{{ route('group.becomeMember', ['id' => $group->id]) }}">Lid worden.</a>
                                @elseif($group->group_type_id === 2)
                                    <a href="{{ route('group.becomeMember', ['id' => $group->id]) }}">Aanvraag plaatsen.</a>
                                @else
                                    Je moet uitgenodigd worden.
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>

    {{-- Modal for creating new group --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="createGroupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Nieuwe groep aanmaken</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('group.new') }}">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label>Groep naam</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Groep locatie</label>
                            <input type="text" name="location" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label>Groeps beschrijving</label>
                            <textarea class="form-control" name="description" rows="5" required></textarea>
                        </div>

                        <div class="form-group">
                            <label>Groep type</label>
                            <select name="type" class="form-control" required>
                                @foreach($group_types as $type)
                                    <option value="{{ $type->id }}">{{ $type->type }}: {{ $type->description }}</option>
                                @endforeach
                            </select>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    <button type="submit" class="btn btn-primary">Opslaan</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection