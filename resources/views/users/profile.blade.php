@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">{{ $user->fname }} {{ $user->sname }}</div>

            <div class="panel-body">
                <div class="col-md-6">
                    @if(Auth::user() == $user)
                        <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="control-label">Email</label>

                                <div class="">
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="control-label">Telefoonnummer</label>

                                <div class="">
                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
                                <label class="control-label">Postcode</label>

                                <div class="">
                                    <input type="text" class="form-control" name="zip_code" value="{{ $user->zip_code }}">

                                    @if ($errors->has('zip_code'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('adress') ? ' has-error' : '' }}">
                                <label class="control-label">Adres</label>

                                <div class="">
                                    <input type="text" class="form-control" name="adress" value="{{ $user->adress }}">

                                    @if ($errors->has('adress'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('adress') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                <label class="control-label">Woonplaats</label>

                                <div class="">
                                    <input type="text" class="form-control" name="city" value="{{ $user->city }}">

                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="pf">Profiel afbeelding</label>
                                <input type="file" name="image" class="form-control" id="pf">
                            </div>

                            <button class="btn btn-primary" type="submit">Opslaan</button>
                        </form>
                    @else
                        <ul class="col-md-6">
                            <li>Email: {{ $user->email }}</li>
                            <li>Woonplaats: {{ $user->city }}</li>
                            <li>Geboortedatum: {{ $user->dob }}</li>
                        </ul>

                        <ul class="col-md-6">
                            <li><strong>Groepen waar {{ $user->fname }} lid van is:</strong></li>
                            @foreach($user->groups as $group)
                                <li><a href="{{ route('group.detail', ['id' => $group->id]) }}">{{ $group->name }}</a></li>
                            @endforeach
                        </ul>
                    @endif

                </div>
                @if (Storage::disk('local')->has($user->fname . '-' . $user->id . '.jpg'))
                    <div class="col-md-6">
                        <img class="col-md-6" src="{{ route('profile.image', ['filename' => $user->fname . '-' . $user->id . '.jpg']) }}" alt="" class="img-responsive">
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
